import React, { Component } from 'react';
import "../css/bootstrap.css";
import Title from "./title.component";
import theme from "../services/theme";
import {Redirect} from "react-router-dom";
import icons from "../services/icons";

class Top extends Component {

    constructor(){
        super();
        this.state = {redirect: false}
    }

    handleLogout(){
        localStorage.removeItem("user_id")
        this.setState({redirect: true})
    }

    render() {

        if(this.state.redirect){
            return(
                <Redirect to="/"></Redirect>
            )
        }else{
            return (
                <nav className="navbar navbar-dark pl-0" style={theme.deepBg}>
                    <button className="float-left btn btn-transparent" onClick={this.props.menu}>{icons.menu("#fff", 25)}</button> <Title size={"30px"} color={"#fff"}/>
                    <div className="float-right btn-group">
                        <button className="btn float-left" style={theme.mainBtn} onClick={this.handleLogout.bind(this)}>Logout</button>
                        <button className="btn float-left" style={theme.darkBtn}><i>{icons.settings("#fff")}</i></button>
                    </div>
                </nav>
            );
        }
    }
  }

export default Top
