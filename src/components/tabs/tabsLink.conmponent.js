import React, { Component } from 'react';
import "../../css/bootstrap.css"
import icons from "../../services/icons"

class TabLink extends Component {
    constructor(){
        super();
    }

    render(){
        return (
            <a
                className={`nav-item nav-link ${this.props.active ? "active" : ""}`}
                id={this.props.labelledby}
                data-toggle="tab"
                href={`#${this.props.id}`}
                role="tab"
                aria-controls={this.props.id}
                aria-selected="true">
                    {this.props.title}
            </a>
        )
    }
}

export default TabLink
