import React, { Component } from 'react';
import "../../css/bootstrap.css"
import icons from "../../services/icons"

class TabContent extends Component {
    constructor(){
        super();
    }

    render(){
        return(
            <div className={'tab-pane fade col-sm-12 ' + (this.props.show ? "show" : "") + (this.props.active ? " active" : "")} id={this.props.id} role="tabpanel" aria-labelledby={this.props.labelledby}>
                <div class="row pt-3">
                    <div className="col-sm-12">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default TabContent
