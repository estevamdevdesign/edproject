import React, { Component } from 'react';
import {Link} from "react-router-dom";
import "../css/bootstrap.css"

class Top extends Component {

    constructor(){
        super();
        this.state = {}
    }

    render() {
        return (
            <h1 className="text-center font-weight-light" style={{fontSize: this.props.size, color: this.props.color}}>ED <span className="font-weight-bold">Project</span></h1>
        );
    }
  }

export default Top
