import React, { Component } from 'react';
import Input from  "./form/input.component";
import TextArea from  "./form/textarea.component";
import InputMask from  "react-input-mask";
import Select from 'react-select';
import AsyncSelect from 'react-select/lib/Async';
import icons from "../services/icons";
import "../css.component/form.component.css";
import crud from "../services/crud";
import {Redirect} from "react-router-dom";

// let data = {}
// let options = crud.get("states").then((response) =>{
//     return response.data
// })

class Form extends Component {
    constructor(){
        super();
        this.state={}
        this.handleSubmit = this.handleSubmit.bind(this)
        // this.handleLoadOptions = this.handleLoadOptions.bind(this)
    }

    handleSubmit(event){
        event.preventDefault()
        console.log(this.props.data)
        // crud.post(this.props.api, this.state).then(() =>{
        //     this.setState({saved: true})
        //   }).catch((e) =>{
        //     console.log(e)
        // })
    }

    handleDelete(id, api, event){
        event.preventDefault()
        console.log(id, api)
        // crud.post(this.props.api, this.state).then(() =>{
        //     this.setState({saved: true})
        //   }).catch((e) =>{
        //     console.log(e)
        // })
    }

    // handleSelect(newValue, name, bind){
    //     this.setState({[name] : newValue}, () =>{
    //         bind && bind.map((data) =>{
    //             this.setState({[data.field] : newValue[data.value]})
    //         })
    //     })
    // }

    // handleLoadOptions(options){
    //     console.log("options")
    // }

    // componentWillMount(){
    //     this.setState(data)
    // }

    render() {

        if(this.state.saved){
            return(<Redirect to={`/${this.props.redirect}`}/>)
        }
        else{
            return (
                <form>
                    <div className="row">
                        {this.props.children}
                    </div>
                    <hr />
                    <div className="btn-group">
                        {this.props.submitButton && <button className="btn main-btn" onClick={this.handleSubmit.bind(this)}>{icons[this.props.iconSubmit](this.props.iconColor)} {this.props.submitButton}</button>}
                        {this.props.deleteButton && <button className="btn btn-danger" onClick={this.handleDelete.bind(this, this.props.api, "1234")}>{icons[this.props.iconDelete](this.props.iconColor)} {this.props.deleteButton}</button>}
                    </div>
                </form>
                // <form onSubmit={this.handleSubmit} >
                //     <div className="row">
                //         {this.props.formConf.map((field) =>{
                //             if((field.type == "text" || field.type == "date" || field.type == "number" || field.type == "radio" || field.type == "email" || field.type == "password") && !field.mask){
                //                 return(
                //                     <Input
                //                         size={field.size}
                //                         id={field.id}
                //                         name={field.name}
                //                         value={this.state && this.state[field.name]}
                //                         placeholder={field.placeholder}
                //                         required={field.required}
                //                         readOnly={field.readOnly}
                //                         disabled={field.dependency && !this.state[field.dependency]}
                //                         height={field.height}
                //                         type={field.type}
                //                         onChange={(event) => this.setState({[field.name] : event.target.value})}
                //                         label={field.label}
                //                     />
                //                 )
                //             }else if(field.mask){
                //                 return(
                //                     <div className={field.size + " form-group mb-3"} key={this.props.id}>
                //                         <label className="label-form" for={field.id}>{field.label}</label>
                //                         <InputMask
                //                             mask={field.mask}
                //                             className="form-control"
                //                             size={field.size}
                //                             id={field.id}
                //                             name={field.name}
                //                             value={this.state && this.state[field.name]}
                //                             placeholder={field.placeholder}
                //                             required={field.required}
                //                             readOnly={field.readOnly}
                //                             type={field.type}
                //                             onChange={(event) => this.setState({[field.name] : event.target.value})}
                //                             label={field.label}
                //                         />
                //                     </div>
                //                 )
                //             }else if(field.type == "autoComplete"){
                //                 return(
                //                     <div className={field.size + " form-group mb-3"} key={this.props.id}>
                //                         <label className="label-form" for={field.id}>{field.label}</label>
                //                         <Select
                //                             options={field.options}
                //                             // loadOptions={this.handleLoadOptions}
                //                             size={field.size}
                //                             value={this.state && this.state[field.name]}
                //                             defaultValue={this.state && this.state[field.name]}
                //                             onChange={(newValue) =>{this.handleSelect(newValue, field.name, field.dataBind)}}
                //                             isDisabled={field.disabled}
                //                         />
                //                     </div>
                //                 )
                //             }else if(field.type == "textarea"){
                //                 return(
                //                     <TextArea
                //                         size={field.size}
                //                         id={field.id}
                //                         name={field.name}
                //                         value={this.state && this.state[field.name]}
                //                         placeholder={field.placeholder}
                //                         required={field.required}
                //                         readOnly={field.readOnly}
                //                         disabled={field.dependency && !this.state[field.dependency]}
                //                         height={field.height}
                //                         type={field.type}
                //                         onChange={(event) => this.setState({[field.name] : event.target.value})}
                //                         label={field.label}
                //                     />
                //                 )
                //             }else if(field.type == "space"){
                //                 return(
                //                     <div className="container-fluid">
                //                         <hr className="my-3"/>
                //                         <h5 className="font-weight-light">{field.fieldTitle}</h5>
                //                     </div>
                //                 )
                //             }
                //         })}
                //     </div>
                //     <hr />
                //     <div className="btn-group">
                //         {this.props.submitButton && <button className="btn main-btn">{icons[this.props.iconSubmit](this.props.iconColor)} {this.props.submitButton}</button>}
                //         {this.props.deleteButton && <button className="btn btn-danger" onClick={this.handleDelete.bind(this, 1234)}>{icons[this.props.iconDelete](this.props.iconColor)} {this.props.deleteButton}</button>}
                //     </div>
                // </form>
            );

        }

    }
}

export default Form;
