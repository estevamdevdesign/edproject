import React, { Component } from 'react';
import "../css/bootstrap.css"
import TabLink from "./tabs/tabsLink.conmponent"
import icons from "../services/icons"

class Tab extends Component {

    constructor(){
        super();
        this.state = {}
    }


    render() {

        return (
            <div>
                <div className="row">
                    <div className="col-sm-12">
                        <nav>
                            <div class="nav nav-tabs" id={this.props.id} role="tablist">
                                {this.props.tabLinks.map((item) => {
                                    return (
                                        <TabLink title={item.title} id={item.id} active={item.active}/>
                                    )
                                })}
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="tab-content row" id="nav-tabContent">
                    {this.props.children}
                </div>
            </div>
        );
    }
  }

export default Tab
