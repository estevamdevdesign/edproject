import React, { Component } from 'react';

class Input extends Component {
    constructor(){
        super();
        this.state={}
    }

    render() {
        return (
            <div className={this.props.size + " form-group mb-3 d-flex justify-content-start align-items-center"}>
                <label className="label-form float-left">{this.props.label}</label>
                <input 
                    type="radio"
                    id={this.props.id}
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    required={this.props.required}
                    readOnly={this.props.readOnly}
                    value={this.props.value}
                    disabled={this.props.disabled}
                    className="ml-3 form-control"
                    style={{height: 20}}
                    checked={this.props.checked}
                    onChange={this.props.onChange}
                    onLoad={this.props.onLoad}
                />
            </div>
        );
    }
}

export default Input;
