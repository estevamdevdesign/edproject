import React, { Component } from 'react';

class Form extends Component {
    constructor(){
        super();
        this.state={}
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount(){
        this.setState({data: this.props.data})
    }

    handleSubmit(event){
        event.preventDefault()
        console.log(this.state.data)
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} >
                {this.props.children}
            </form>
        );
    }
}

export default Form;
