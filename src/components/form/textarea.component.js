import React, { Component } from 'react';

class Input extends Component {
    constructor(){
        super();
        this.state={}
    }

    render() {
        return (
            <div className={this.props.size + " form-group mb-3"}>
                <label className="label-form">{this.props.label}</label>
                <textarea 
                    type={this.props.type}
                    id={this.props.id}
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    required={this.props.required}
                    readOnly={this.props.readOnly}
                    value={this.props.value}
                    className="form-control"
                    onChange={this.props.onChange}
                    style={{height: this.props.height}}
                ></textarea>
            </div>
        );
    }
}

export default Input;
