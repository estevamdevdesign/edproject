import React, { Component } from 'react';
import Select from 'react-select';
import crud from '../../services/crud';

class AutoComplete extends Component {
    constructor(){
        super();
        this.state={}
    }

    componentWillMount(){
        crud.get(this.props.data).then((response) =>{
            if(!this.state.options){
                this.setState({options: response.data})
            }
        })
    }

    render() {
        return(
            <div className={this.props.size + " form-group mb-3"} key={this.props.id}>
                <label className="label-form" >{this.props.label}</label>
                <Select
                    options={this.state.options}
                    size={this.props.size}
                    value={this.props && this.props.value}
                    defaultValue={this.state && this.state.options && this.state.options[this.props.defaultValue]}
                    onChange={this.props.onChange}
                    isDisabled={this.props.disabled}
                />
            </div>
        )
    }
}

export default AutoComplete;
