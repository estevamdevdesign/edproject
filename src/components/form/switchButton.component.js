import React, { Component } from 'react';
import '../../css.component/switchButton.component.css'

// let switchButton = {
//     border: "1px solid #ffcc00",
//     background: "transparent",
// };

// let activedSwitchButton = {
//     border: "1px solid transparent",
//     background: "#fc0",
// };

class SwitchButton extends Component {
    constructor(){
        super();
        this.state={}
    }

    render() {
        return(
            <div className={this.props.size + " mb-3"} key={this.props.id}>
                <label className="label-form">{this.props.label}</label>
                <div className="col-sm-12">
                    <div className="row">
                        <div className={"col-sm-6 switchButton"} onClick={this.props.onTrueClick} style={this.props.value ? {background: this.props.activeColor, border: '0px solid transparent', color: "#fff"} : {background: 'transparent', border: '1px solid ' + this.props.activeColor}}>{this.props.true}</div>
                        <div className={"col-sm-6 switchButton"} onClick={this.props.onFalseClick} style={this.props.value === false ? {background: this.props.activeColor, border: '0px solid transparent', color: "#fff"} : {background: 'transparent', border: '1px solid ' + this.props.activeColor}}>{this.props.false}</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SwitchButton;
