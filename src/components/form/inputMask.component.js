import React, { Component } from 'react';
import InputMask from  "react-input-mask";

class inputMask extends Component {
    constructor(){
        super();
        this.state={}
    }

    render() {
        return(
            <div className={this.props.size + " form-group mb-3"} key={this.props.id}>
                <label className="label-form">{this.props.label}</label>
                <InputMask
                    mask={this.props.mask}
                    className="form-control"
                    size={this.props.size}
                    id={this.props.id}
                    name={this.props.name}
                    value={this.props.value}
                    placeholder={this.props.placeholder}
                    required={this.props.required}
                    readOnly={this.props.readOnly}
                    type={this.props.type}
                    onChange={this.props.onChange}
                    onLoad={this.props.onLoad}
                    label={this.props.label}
                />
            </div>
        )
    }
}

export default inputMask;
