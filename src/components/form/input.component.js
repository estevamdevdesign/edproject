import React, { Component } from 'react';

class Input extends Component {
    constructor(){
        super();
        this.state={}
    }

    render() {
        return (
            <div className={this.props.size + " form-group mb-3"}>
                <label className="label-form">{this.props.label}</label>
                <input 
                    type={this.props.type}
                    id={this.props.id}
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    required={this.props.required}
                    readOnly={this.props.readOnly}
                    value={this.props.value}
                    disabled={this.props.disabled}
                    className="form-control"
                    style={this.props.type == "radio" ? {height: 20} : {height: this.props.type}}
                    onChange={this.props.onChange}
                    onLoad={this.props.onLoad}
                />
            </div>
        );
    }
}

export default Input;
