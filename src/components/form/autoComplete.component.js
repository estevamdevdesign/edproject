import React, { Component } from 'react';
import Select from 'react-select';

class AutoComplete extends Component {
    constructor(){
        super();
        this.state={}
    }

    render() {
        return(
            <div className={this.props.size + " form-group mb-3"} key={this.props.id}>
                <label className="label-form" >{this.props.label}</label>
                <Select
                    options={this.props.options}
                    // loadOptions={this.props.loadOptions}
                    size={this.props.size}
                    value={this.props && this.props.value}
                    defaultValue={this.props && this.props.options[this.props.defaultValue]}
                    onChange={this.props.onChange}
                    // onChange={(newValue) =>{this.handleSelect(newValue, this.props.name, this.props.dataBind)}}
                    isDisabled={this.props.disabled}
                />
            </div>
        )
    }
}

export default AutoComplete;
