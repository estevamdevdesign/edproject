import React, { Component } from 'react';
import Menu from "./menu.component"
import "../css/bootstrap.css"
import Top from "./top.component"

class Body extends Component {

    constructor(){
        super();
        this.state = {
            showMenu: false
        }
    }

    handleMenu(){
        if(!this.state.showMenu){
            this.setState({showMenu: true})
        }else{
            this.setState({showMenu: false})
        }
    }

    render() {
        return (
            <div>
            <Top menu={this.handleMenu.bind(this)}/>
                <div className="container-fluid px-0 mx-0">
                    <div className="row-fluid">
                        <div className="row no-gutters">
                            <div className={this.state.showMenu ? "col-sm-2 mediun-dark-bg main-menu" : "d-none"}>
                                <Menu/>
                            </div>
                            <div className={this.state.showMenu ? "col-sm-10 pt-5 bg-light main-body" : "col-sm-12 pt-5 bg-light main-body"}>
                                <div className="container-fluid pt-4">
                                    {this.props.children}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
  }

export default Body
