import React, { Component } from 'react';
import "../css/bootstrap.css";
import {Link} from 'react-router-dom';
import icons from "../services/icons"

class List extends Component {

    constructor(){
        super();
        this.state = {}
    }

    render() {
        return (
            <div>
                {this.props.data.map((item) =>{
                    return (
                        <div className="row py-3 border-bottom" key={item.id}>
                            <div className="col-sm-7">
                                {item.title}
                            </div>
                            <div className="col-sm-3">
                                {item.dataConclusao}
                            </div>
                            <div className="col-2">
                                <Link to={`/atividades/${item.id}`} className="btn btn-primary">Ver</Link>
                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }
  }

export default List
