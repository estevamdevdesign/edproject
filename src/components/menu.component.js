import React, { Component } from 'react';
import icons from "../services/icons";
import theme from "../services/theme";
import {Link} from 'react-router-dom';
import "../css/bootstrap.css";

class Menu extends Component {

    constructor(){
        super();
        this.state = {}
    }

    componentDidMount(){
        let links = [
            {label: "Dashboard", url: "/dashboard", id: 9, icon: icons["dataUsage"]("#8069f2", 20), dropdown: false},
            {label: "Folders", url: "", id: 1, icon: icons["folder"]("#8069f2", 20), dropdown: true, name: "folders"},
            {label: "All Folders", url: "/folders", id: 2, icon: icons["folderOpen"]("#8069f2", 20), dropdown: false, parent_name: "folders"},
            {label: "Create a folder", url: "/add-folders", id: 3, icon: icons["createNewFolder"]("#8069f2", 20), dropdown: false, parent_name: "folders"},
            {label: "Components", url: "/components", id: 5, icon: icons["build"]("#8069f2", 20), dropdown: false, name: "components"},
            {label: "Profile", url: "/profile", id: 4, icon: icons["accountCircle"]("#8069f2", 20), dropdown: false, name: "profile"},
        ]
        this.setState({links: links})
    }

    render() {
        return (
            <ul className="navbar-nav mr-auto">
                {this.state && this.props.links && this.props.links.map((link, index) =>{ 
                    return (<li className="nav-item" key={index}>
                        {link.dropdown && !link.hide &&
                            <div className="accordion" id={link.name}>
                                <div className="" id={"heading" + link.name}>
                                    <a className={'dropdown-toggle drop-menu'} style={theme.navLink} data-toggle="collapse" data-target={"#collapse" + link.name} aria-expanded="true" aria-controls={"#collapse" + link.name} >
                                        {link.icon} <span className="ml-2">{link.label}</span>
                                    </a>
                                </div>
                                <div id={"collapse" + link.name} className="collapse menu-collapse" aria-labelledby="headingOne" data-parent={"#" + link.name}>
                                    {this.props.links.map((children, i) =>{
                                        if(children.parent_name == link.name){
                                            return (
                                                <Link key={i} style={theme.navLink} to={children.path}>{children.icon} <span className="ml-2">{children.label}</span> </Link>
                                            )
                                        }
                                    })}
                                </div>
                            </div>
                        }
                        {!link.parent_name && !link.dropdown && !link.hide &&<Link key={link.index} style={theme.navLink} to={link.path}>{link.icon} <span className="ml-2">{link.label}</span> </Link>}
                    </li>)
                })}
            </ul>
        );
    }
  }

export default Menu
