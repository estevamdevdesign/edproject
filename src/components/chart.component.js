import React, { Component } from 'react';
import "../css/bootstrap.css";
import icons from "../services/icons";
import theme from "../services/theme";
import {LineChart, Line, Tooltip, Legend, ResponsiveContainer, PieChart, Pie, Cell, BarChart, Bar} from "recharts";
import "../css.component/chart.component.css"

const dataAno =[
  {name: 'Jan', ch: 1, cn: 2.4, lc: 9.4, m: 3},
  {name: 'Fev', ch: 2, cn: 3.4, lc: 8.4, m: 2},
  {name: 'Mar', ch: 3, cn: 4.4, lc: 7.4, m: 1},
  {name: 'Abr', ch: 4, cn: 5.4, lc: 6.4, m: 9},
  {name: 'Mai', ch: 5, cn: 6.4, lc: 5.4, m: 8},
  {name: 'Jun', ch: 6, cn: 7.4, lc: 4.4, m: 7},
  {name: 'Jul', ch: 7, cn: 8.4, lc: 3.4, m: 6},
  {name: 'Ago', ch: 8, cn: 9.4, lc: 2.4, m: 5},
  {name: 'Set', ch: 9, cn: 1.4, lc: 1.4, m: 4},
  {name: 'Out', ch: 1, cn: 2.4, lc: 9.4, m: 3},
  {name: 'Nov', ch: 2, cn: 3.4, lc: 8.4, m: 2},
  {name: 'Dez', ch: 3, cn: 4.4, lc: 7.4, m: 1},
];

const dataSem1 =[
  {name: 'Jan', ch: 1, cn: 2.4, lc: 9.4, m: 3},
  {name: 'Fev', ch: 2, cn: 3.4, lc: 8.4, m: 2},
  {name: 'Mar', ch: 3, cn: 4.4, lc: 7.4, m: 1},
  {name: 'Abr', ch: 4, cn: 5.4, lc: 6.4, m: 9},
  {name: 'Mai', ch: 5, cn: 6.4, lc: 5.4, m: 8},
  {name: 'Jun', ch: 6, cn: 7.4, lc: 4.4, m: 7}
];

const dataSem2 =[
  {name: 'Jul', ch: 7, cn: 8.4, lc: 3.4, m: 6},
  {name: 'Ago', ch: 8, cn: 9.4, lc: 2.4, m: 5},
  {name: 'Set', ch: 9, cn: 1.4, lc: 1.4, m: 4},
  {name: 'Out', ch: 1, cn: 2.4, lc: 9.4, m: 3},
  {name: 'Nov', ch: 2, cn: 3.4, lc: 8.4, m: 2},
  {name: 'Dez', ch: 3, cn: 4.4, lc: 7.4, m: 1}
];

  // const getIntroOfPage = (label) => {
  //   // if (label === 'Page A') {
  //   //   return "Page A is about men's clothing";
  //   // } if (label === 'Page B') {
  //   //   return "Page B is about women's dress";
  //   // } if (label === 'Page C') {
  //   //   return "Page C is about women's bag";
  //   // } if (label === 'Page D') {
  //   //   return 'Page D is about household goods';
  //   // } if (label === 'Page E') {
  //   //   return 'Page E is about food';
  //   // } if (label === 'Page F') {
  //   //   return 'Page F is about baby food';
  //   // }

  //   return label
  // };
  
  const CustomTooltip = ({ active, payload, label }) => {

    if (active) {
      return (
        <div className="custom-tooltip">
          <p className="label">{`${payload ? payload[0].payload.name : ""} : ${payload ? payload[0].value : ""}`}</p>
        </div>
      );
    }
  
    return null;
  };

class Chart extends Component {

    constructor(){
        super();
        this.state = {subTitle: "", data: []}
    }

    handleFilter(filter){
      switch(filter){
        case "1º Semestre" :
          this.setState({data: dataSem1, subTitle: filter})
          break;
        
        case "2º Semestre" :
          this.setState({data: dataSem2, subTitle: filter})
          break;
  
        case "Ano" :
          this.setState({data: dataAno, subTitle: filter})
          break;
      }
    }

    componentDidMount(){
      if(this.props.chartDefault){
        this.handleFilter(this.props.chartDefault)
      }
    }

    render() {

      return (
            <div className={this.props.size + " mb-3"}>
                <div className={"shadow-sm border-0 card col-sm-12 py-3"} style={theme[this.props.bgColor]}>
                    <div className="">
                        {this.props.title && <h5 className={"card-title float-left " + this.props.color}>{this.props.title}</h5>}
                        {/* {this.props.haveFilter && */}
                        {
                          <div class="dropdown">
                            <button class="btn btn-transparent btn-sm float-right" type="button" id={`drop${this.props.id}`} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              {icons.moreHoriz("#333", 25)}
                            </button>
                            <div class="dropdown-menu" aria-labelledby={`drop${this.props.id}`}>
                              <a class="dropdown-item" onClick={this.handleFilter.bind(this, 'Ano')}>Ano</a>
                              <a class="dropdown-item" onClick={this.handleFilter.bind(this, '1º Semestre')}>1º Semestre</a>
                              <a class="dropdown-item" onClick={this.handleFilter.bind(this, '2º Semestre')}>2º Semestre</a>
                            </div>
                          </div>
                        }
                    </div>

                    {/* <button className="btn btn-transparent btn-sm border float-right">{icons.moreHoriz("#fff", 20)}</button> */}
                    
                    {this.state.subTitle && <h6 className={"card-subtitle mb-2 small " + this.props.subColor}>{this.state.subTitle}</h6>}
                    
                    <div style={{ width: '100%', height: this.props.chartHeight }}>
                        {this.props.chart && this.props.chart == "LineChart" &&
                            <ResponsiveContainer>
                                <LineChart height={100} data={this.state.data}>
                                    {this.props.chartDataKeyList && this.props.chartDataKeyList.map((key, index) =>{
                                        return(
                                            <Line type="monotone" dataKey={key.filter} stroke={key.color} key={index} strokeWidth={this.props.strokeWidth} />
                                        )
                                    })}
                                    {this.props.chartTooltip && <Tooltip content={<CustomTooltip />} />}
                                    {this.props.chartLegend && <Legend />}
                                </LineChart>
                            </ResponsiveContainer>
                        }

                        {this.props.chart && this.props.chart == "PieChart" &&
                            <ResponsiveContainer>
                                <PieChart>
                                    <Pie stroke="transparent" dataKey={this.props.chartDataKey} data={this.props.chartData} label innerRadius={60} paddingAngle={5} labelLine={false}>
                                        {this.props.chartData.map((entry, index) => <Cell key={`cell-${index}`} fill={this.props.chartColor[index % this.props.chartColor.length]} />)}
                                    </Pie>
                                </PieChart>
                            </ResponsiveContainer>
                        }

                        {this.props.chart && this.props.chart == "BarChart" &&
                            <ResponsiveContainer>
                                <BarChart width={150} height={40} data={this.props.chartData}>
                                    <Bar dataKey={this.props.chartDataKey} fill={this.props.chartFillColor} />
                                    {this.props.chartTooltip && <Tooltip content={<CustomTooltip />} />}
                                    {this.props.chartLegend && <Legend />}
                                </BarChart>
                            </ResponsiveContainer>
                        }
                    </div>
                </div>
            </div>
            
        );
    }
  }

export default Chart
