import Folders from './folders'

const plugins = [
    {name: "Folders", Folders, plugin_description: "File manager to React and Node application."},
    
]

export default plugins;