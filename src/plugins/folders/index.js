import React from 'react';
import Folders from './views/folders';
import Folder from './views/folder';
import AddFolder from './views/add-folder';
import icons from '../../services/icons'

const routes = [
    {label: "Folders", component: "", icon: icons["folder"]("#8069f2", 20), dropdown: true, name: "folders"},
    {path: "/folders", component: "Folders", label: "All folders", icon: icons["folderOpen"](), dropdown: false, parent_name: "folders"},
    {path: "/add-folder", component: "AddFolder", label: "Add folder", icon: icons["createNewFolder"](), dropdown: false, parent_name: "folders"},
    {path: "/folders/:id", component: "Folder", label: "Folder", icon: icons["folderOpen"](), hide: true},
]

const components =[
    {name: "AddFolder", component: (link, props, getUser, icons, url, crud) => <AddFolder {...props} user_id={getUser} icons={icons} url={url} crud={crud}/>},
    {name: "Folders", component: (link, props, getUser, icons, url, crud) => <Folders {...props} user_id={getUser} icons={icons} url={url} crud={crud}/>},
    {name: "Folder", component: (link, props, getUser, icons, url, crud) => <Folder {...props} user_id={getUser} icons={icons} url={url} crud={crud}/>},
] 

export default [routes, components];