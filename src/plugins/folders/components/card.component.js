import React, { Component } from 'react';
import "../css/bootstrap.css"
// import icons from "../services/icons"

class Top extends Component {

    constructor(){
        super();
        this.state = {}
    }

    render() {
        return (
            <div className={this.props.size + " mb-3"} id={this.props.id}>
                <div className={"shadow-sm border-0 card col-sm-12 py-3 " + this.props.bgColor} onClick={this.props.onClick} style={{backgroundColor: this.props.background}}>
                    <div className="d-flex align-items-center mb-3">
                        {/* {this.props.icon && <i className="float-left vertical-align mr-3 ">{icons[this.props.icon](this.props.iconColor, this.props.iconSize)}</i>} */}
                        {this.props.title && <h1 className={"card-title float-left m-0 font-weight-light " + this.props.color} style={{fontSize: this.props.titleSize, color: this.props.titleColor}}>{this.props.title}</h1>}
                    </div>
                    {this.props.subTitle && <h6 className={"card-subtitle mb-2 small " + this.props.subColor} style={{fontSize: this.props.subTitleSize, color: this.props.subTitleColor}}>{this.props.subTitle}</h6>}
                    {this.props.children}
                </div>
                {/* {this.props.haveRemove && <button className="btn btn-transparent delete-folder" onClick={this.props.haveRemove}>{icons.remove("#ccc", 30)}</button>} */}
            </div>
            
        );
    }
  }

export default Top
