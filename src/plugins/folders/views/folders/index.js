import React, { Component } from 'react';
import Card from "../../components/card.component";
import "./styles.css";
import {Link, Redirect} from 'react-router-dom';

export default class Folders extends Component {

  constructor(){
    super();
    this.state = {folders: [], folderId: undefined}
  }

  async componentDidMount(){
    await this.props.crud.get(`users/${this.props.user_id}/folders`).then((response) =>{
      this.setState({folders: response.data})
    })
  }

  async handleDeleteFolder(id){
    await this.props.crud.delete(id, `users/${this.props.user_id}/folders`).then((response) =>{
      this.setState({folders : response.data})
    })
  }

  async handleRedirect(id){
    this.setState({redirect: true, folderId: id})
  }

  render() {
    return (
        <div className="">
              <div className="row">
                <Card size="col-sm-12" title="Folders" icon={"folderOpen"} iconColor={"#212529"} iconSize={40}>
                  <div className="row">
                  {this.state && this.state.folders && this.state.folders.map(item =>{
                    return(
                      <Link to={`folders/${item._id}`} className="col-sm-3">
                        <div className="row">
                          <Card size="col-sm-12" key={item._id} title={item.title} titleSize={20} haveRemove={this.handleDeleteFolder.bind(this, item._id)}/>
                        </div>
                      </Link>
                    )
                  })}
                  </div>  
                </Card>
              </div>
        </div>
    );
  }
}
