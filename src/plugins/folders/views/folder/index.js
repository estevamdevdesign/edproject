import React, { Component } from 'react';
import {MdClear} from 'react-icons/md';
import "./styles.css";
import {distanceInWords} from 'date-fns';
import en from 'date-fns/locale/en';
// import url from "../../services/url";
import Dropzone from "react-dropzone";
import socket from "socket.io-client";
// import crud from "../../services/crud"

export default class Folders extends Component {
  
  constructor(){
    super();
    this.state = {title: "", files: []};
    this.handleGetItens = this.handleGetItens.bind(this)
    this.handleDeleteFile = this.handleDeleteFile.bind(this)
  }

  async handleGetItens(){
    await  this.props.crud.getAll(`${this.props.match.params.id}`, `users/${this.props.user_id}/folders`).then((response) =>{
      this.setState({title: response && response.data && response.data.title, files: response && response.data && response.data.files})
    })
  }

  async componentDidMount(){
    this.subscribeToNewFiles();
    await this.handleGetItens();
  }

  handleUpload = (files) =>{
    files.forEach(file =>{
      const data = new FormData();
      data.append('file', file);
      this.props.crud.post(`users/${this.props.user_id}/folders/${this.props.match.params.id}/files`, data)
    })
  }

  subscribeToNewFiles = () =>{
    const folder = this.props.match.params.id
    const io = socket(this.props.url);
    io.emit('connectRoom', folder);
    io.on('file', data => {
      this.setState({files: [data, ...this.state.files]})
    })
  }

  subscribeToDeleteFiles = (id) =>{
    const file = id;
    const io = socket(this.props.url);

    io.emit('connectFileRoom', file);
    io.on('file', data =>{
      this.setState({files: [data, ...this.state.files]})
    })
  }

  async handleDeleteFile(id){
    this.props.crud.delete(id, `users/${this.props.user_id}/folders/${this.props.match.params.id}/files`).then(() =>{
      this.handleGetItens();
    }).catch((e) =>{
      console.log(e)
    });
  }
  
  render() {
    return (
        <div className="">
            {/* <Body> */}
              <div className="row">
                <div className="col-sm-12">
                  <h2 className="display-4 text-center">{this.state.title}</h2>
                </div>
              </div>
              <div className="row my-2">
                <Dropzone onDropAccepted={this.handleUpload}>
                  {({getRootProps, getInputProps}) => (
                    <div className="col-sm-12" {...getRootProps()}>
                      <div className="row-fluid p-5 border text-center">
                        <input {...getInputProps()} />
                        <p className="small text-center m-0">Drag and drop your files here or click here.</p>
                      </div>
                    </div>
                  )}
                </Dropzone>
              </div>
              <div className="row my-3">
                <div className="col-sm-12">
                  <div className="row-fluid bg-white p-3">
                    <div className="row">
                      <div className="col-sm-3">
                        TODO Filter
                      </div>
                      <div className="col-sm-3">
                        TODO Filter
                      </div>
                      <div className="col-sm-3">
                        TODO Filter
                      </div>
                      <div className="col-sm-3">
                        TODO Filter
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <ul className="nav row">
                    {this.state.files && this.state.files.map((file) =>{
                      return (
                        <li className="col-sm-2 my-2" key={file.id}>
                          <div className="row-fluid text-right p-3 shadow-sm">
                            <button className="btn btn-default btn-sm d-inline-block" onClick={this.handleDeleteFile.bind(this, file._id)}>
                              <MdClear size={20} color="#aaa" />
                            </button>
                            <p className="text-center">
                              <a href={file.url} target="_blank">
                                {/* <MdInsertDriveFile size={60} color="#a5cfff" /> */}
                                <img src={file.url} className="img-fluid" />
                                <p className="small">{file.title}</p>
                                <p className="small">{distanceInWords(file.createdAt, new Date(), {locale: en})} ago</p>
                              </a>
                            </p>
                          </div>
                          
                        </li>
                      )
                    })}
                  </ul>
                </div>
              </div>
            {/* </Body> */}
        </div>
    );
  }
}
