import React, { Component } from 'react';
import './App.css';
import Top from './components/top.component'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Top />
        <h1 className="display-4">Hello World</h1>
      </div>
    );
  }
}

export default App;
