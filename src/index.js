import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';

//Import Core Components
import Register from './pages/register';
import Login from './pages/login';
import Dashboard from './pages/dashboard';
import History from './pages/history';
import Components from './pages/components';
import Profile from './pages/profile';
import Plugins from './pages/plugins';
import FourHundredFour from './pages/404';
import Top from './components/top.component';
import Menu from './components/menu.component';

//Import Plugins
import plugins from "./plugins/";

//Import Core Services
import icons from "./services/icons";
import url from "./services/url";
import crud from "./services/crud";
import theme from "./services/theme";

//Import Core CSS
import "./css/main.css";

let getUser;

function getUserAuth (next, replace){
    getUser = localStorage.getItem('user_id');
    if(getUser != null){
        return true;
    }else{
        // return true;
        localStorage.setItem('msg-auth', 'Você precisa logar para acessar a timeline')
    }
}

let routelist =[
    {path: "/dashboard", component: "Dashboard", label: "Dashboard", icon: icons["dataUsage"](), dropdown: false},
    {path: "/atividades", component: "Atividades", label: "Atividades", icon: icons["accountCircle"]()},
    {path: "/atividades/:id", component: "Atividades", hide: true},
    {path: "/historico", component: "Historico", label: "Historico", icon: icons["accountCircle"]()},
    // {path: "/components", component: "Components", label: "Components", icon: icons["build"]()},
    // {path: "/profile", component: "Profile", label: "Profile", icon: icons["accountCircle"]()},
    // {path: "/plugins", component: "Plugins", label: "Plugins", icon: icons["dashboard"](), dropdown: false},
]

let componentList =[
    {name: "Dashboard", component: (link, props, getUser) => <Dashboard {...props} user_id={getUser} theme={theme}/>},
    {name: "History", component: (link, props, getUser) => <History {...props} user_id={getUser} theme={theme}/>},
    // {name: "Components", component: (link, props, getUser) => <Components {...props} user_id={getUser}/>},
    // {name: "Profile", component: (link, props, getUser) => <Profile {...props} user_id={getUser}/>},
    // {name: "Plugins", component: (link, props, getUser) => <Plugins {...props} plugins={plugins} user_id={getUser}/>},

]

function getPlugins(){
    plugins.map((plugin)=>{
        plugin[plugin.name][0].map((route)=>{
            if(!route.none){
                routelist.push(route)
            }
        })
        plugin[plugin.name][1].map((component)=>{
            componentList.push(component)
        })
    })
}

class Base extends Component{
    constructor(){
        super();
        this.state={}
    }

    handleMenu(){
        if(!this.state.showMenu){
            this.setState({showMenu: true})
        }else{
            this.setState({showMenu: false})
        }
    }

    render(){
        return(
            <div>
                <Top menu={this.handleMenu.bind(this)}/>
                <div className="container-fluid px-0 mx-0">
                    <div className="row-fluid">
                        <div className="row no-gutters">
                            <div className={this.state.showMenu ? "col-sm-2 main-menu" : "d-none"} style={theme.darkBg}>
                                <Menu links={routelist}/>
                            </div>
                            <div className={this.state.showMenu ? "col-sm-10 pt-5 bg-light main-body" : "col-sm-12 pt-5 bg-light main-body"}>
                                <div className="container-fluid pt-4">
                                    {this.props.children}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function redirectTo(link, props){
    return componentList.map((comp, index) =>{
        if(comp.name == link){
            return(
                getUserAuth() ? <Base key={index}>{comp.component(link, props, getUser, icons, url, crud)}</Base> : <Redirect to="/"/>
            )
        }
    })
}


ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={Login} />
            <Route path="/register" exact={true} component={Register} />

            {/* {getPlugins()} */}

            {routelist.map((item, index) =>{
                if(item.component){
                    return (
                        <Route path={item.path} key={index} exact={true} render={(props) => redirectTo(item.component, props)}/>
                    )
                }
            })}

            <Route path="*" exact={true} component={FourHundredFour} />
        </Switch>
    </BrowserRouter>,
    document.getElementById('root')
);

