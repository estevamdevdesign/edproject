import React, { Component } from 'react';
import "./styles.css";
import Card from "../../components/card.component"
import Chart from "../../components/chart.component"
import Tabs from "../../components/tabs.component"
import List from "../../components/list.component"
import TabContent from "../../components/tabs/tabContent.component"

//ch = ciencias humanas
//cn = ciencias da natureza
//lc = linguagens e códigos
//m =  matemática

const dataAno =[
  {name: 'Ano', ch: 4000, cn: 2400, lc: 2400, m: 3000},
];

const dataSem =[
  {name: 'Page A', ch: 4000, cn: 2400, lc: 2400, m: 3000},
  {name: 'Page B', ch: 4000, cn: 2400, lc: 2400, m: 3000},
];

const dataTri =[
  {name: 'Page A', ch: 4000, cn: 2400, lc: 2400, m: 3000},
  {name: 'Page B', ch: 4000, cn: 2400, lc: 2400, m: 3000},
  {name: 'Page C', ch: 3500, cn: 1400, lc: 3000, m: 4000},
];

const dataBin =[
  {name: 'Page A', ch: 4000, cn: 5400, lc: 3000, m: 3000},
  {name: 'Page B', ch: 3000, cn: 2400, lc: 2400, m: 5000},
  {name: 'Page C', ch: 4500, cn: 3400, lc: 5000, m: 3000},
  {name: 'Page D', ch: 2000, cn: 2400, lc: 2400, m: 3000},
];


export default class Main extends Component {

  constructor(){
    super();
    this.state = {data: ""}
  }

  // handleFilter(filter){
  //   switch(filter){
  //     case "Bimestre" :
  //       this.setState({data: dataBin})
  //       break;

  //     case "Trimestre" :
  //       this.setState({data: dataTri})
  //       break;

  //     case "Semestre" :
  //       this.setState({data: dataSem})
  //       break;

  //     case "Ano" :
  //       this.setState({data: dataAno})
  //       break;
  //   }
  // }

  render() {
    return (
      <div className="">
        <div className="row">
          <Card size="col-sm-12" title="Dashboard" icon={"dataUsage"} iconColor={"#212529"} iconSize={40}>
            <Tabs id={"nav-id"}
              tabLinks={[
                {title:"Dashboard", id:"dashboard", active: true, labelledby:"nav-dashboard"},
                {title:"Vocation Analytics", id: "vocation-analytics", labelledby:"nav-vocation-analytics"},
              ]}>
              <TabContent show={true} active={true} id={"dashboard"} labelledby={"nav-dashboard"}>
                <div className="row">
                  <Chart size="col-sm-3"
                    title="Matemática"
                    id="matematica"
                    bgColor="mainBg"
                    chart="LineChart"
                    chartDefault="Ano"
                    chartHeight={50}
                    chartTooltip={true}
                    chartLegend={false}
                    chartDataKeyList ={[
                      {filter: 'm', color: "#fff"},
                    ]}
                    haveFilter={true}
                    color="text-white"
                    subColor="text-white">
                  </Chart>
                  <Chart size="col-sm-3"
                    title="Ciências Humanas"
                    id="ciencias_humanas"
                    bgColor="theme_bg_1"
                    chart="LineChart"
                    chartDefault="Ano"
                    chartHeight={50}
                    chartTooltip={true}
                    chartLegend={false}
                    chartDataKeyList ={[
                      {filter: 'ch', color: "#fff"},
                    ]}
                    haveFilter={true}
                    color="text-white"
                    subColor="text-white">
                  </Chart>
                  <Chart size="col-sm-3"
                    title="Ciências da Natureza"
                    bgColor="theme_bg_2"
                    id="ciencias_natureza"
                    chart="LineChart"
                    chartDefault="Ano"
                    chartHeight={50}
                    chartTooltip={true}
                    chartLegend={false}
                    chartDataKeyList ={[
                      {filter: 'cn', color: "#fff"},
                    ]}
                    haveFilter={true}
                    color="text-white"
                    subColor="text-white">
                  </Chart>
                  <Chart size="col-sm-3"
                    title="Linguagens e códigos"
                    bgColor="theme_bg_3"
                    id="linguagens_codigos"
                    chart="LineChart"
                    chartDefault="Ano"
                    chartHeight={50}
                    chartTooltip={true}
                    chartLegend={false}
                    chartDataKeyList ={[
                      {filter: 'lc', color: "#fff"}
                    ]}
                    haveFilter={true}
                    color="text-white"
                    subColor="text-white">
                  </Chart>
                  
                </div>
                <div className="row">
                <Card size="col-sm-6" title="Atividades entregues">
                  <List data={[
                      {title: 'Atividade 1', dataConclusao: '21/06/2019',id: '1'},
                      {title: 'Atividade 2', dataConclusao: '22/06/2019', id: '2'},
                      {title: 'Atividade 3', dataConclusao: '20/06/2019', id: '3'}
                    ]} />
                </Card>
                <Card size="col-sm-6" title="Atividades para entregar">
                  <List data={[
                      {title: 'Atividade 1', dataConclusao: '21/06/2019',id: '1'},
                      {title: 'Atividade 2', dataConclusao: '22/06/2019', id: '2'},
                      {title: 'Atividade 3', dataConclusao: '20/06/2019', id: '3'}
                    ]} />
                </Card>
                </div>
              </TabContent>
              <TabContent show={false} active={false} id={"vocation-analytics"} labelledby={"nav-vocation-analytics"}>
                <div className="row">
                  <Chart size="col-sm-12"
                    title="Line Chart"
                    subTitle="Today"
                    chart="LineChart"
                    chartDefault="Ano"
                    chartHeight={150}
                    chartTooltip={true}
                    chartLegend={true}
                    chartDataKeyList ={[
                      {filter: 'ch', color: "#8069f2"},
                      {filter: 'cn', color: "#0cdcB9"},
                      {filter: 'lc', color: "#ffcc00"},
                      {filter: 'm', color: "#cc0000"}
                    ]}
                    strokeWidth={1}
                    haveFilter={false}
                    >
                  </Chart>
                </div>
              </TabContent>
            </Tabs>
          </Card>
        </div>
      </div>
    ); 
  }
}
