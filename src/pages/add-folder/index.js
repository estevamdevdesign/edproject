import React, { Component } from 'react';
import "./styles.css";
import Body from "../../components/body.component"
import crud from "../../services/crud"
import {Redirect} from "react-router-dom"


export default class Main extends Component {

  constructor(){
    super();
    this.state = {
      newBox : "",
      redirect: false,
    }
  }

  handleSubmit = async (event) =>{
    event.preventDefault();
    const response = await crud.post(`users/${this.props.user_id}/folders`, {
      title: this.state.newBox
    }).then((response) =>{
      this.setState({redirect: true, folder_id: response.data._id})
    })
  }

  handleInputChange = (event) =>{
    this.setState({newBox: event.target.value})
  }

  render() {
    if(this.state.redirect){
      return(
        <Redirect to={"/folders"} {...this.props} />
      )
    }else{
      return (
        <div className="">
          {/* <Body> */}
            <form className="form" onSubmit ={this.handleSubmit}>
              <input className="form-control" onChange={this.handleInputChange} value={this.state.newBox}/>
              <button className ="btn btn-primary">Create Folder</button>
            </form>
          {/* </Body> */}
        </div>
      );
    }
  }
}
