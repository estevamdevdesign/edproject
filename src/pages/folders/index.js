import React, { Component } from 'react';
import Body from "../../components/body.component";
import Card from "../../components/card.component";
import "./styles.css";
import crud from "../../services/crud";
import {Link, Redirect} from 'react-router-dom';

export default class Folders extends Component {

  constructor(){
    super();
    this.state = {folders: [], redirect: false, folderId: undefined}
  }

  async componentDidMount(){
    await crud.get(`users/${this.props.user_id}/folders`).then((response) =>{
      this.setState({folders: response.data})
    })
  }

  async handleDeleteFolder(id){
    await crud.delete(id, `users/${this.props.user_id}/folders`).then((response) =>{
      this.setState({folders : response.data})
    })
  }

  async handleRedirect(id){
    this.setState({redirect: true, folderId: id})
  }

  render() {
    if(this.state.redirect){
      return (
        <Redirect {...this.props} to={`folders/${this.state.folderId}`}/>
      )
    }

    return (
        <div className="">
            {/* <Body> */}
              <div className="row">
                <Card size="col-sm-12" title="Folders" icon={"folderOpen"} iconColor={"#212529"} iconSize={40}>
                  <div className="row">
                  {this.state && this.state.folders && this.state.folders.map(item =>{
                    return(
                      <Card size="col-sm-3" key={item._id} title={item.title} titleSize={20} onClick={this.handleRedirect.bind(this, item._id)} haveRemove={this.handleDeleteFolder.bind(this, item._id)}>
                        {/* <div className="row-fluid shadow-sm">
                          <div className="row-fluid bg-light">
                            <div className="row">
                              <div className="col-sm-6">
                                <p className="m-0 small mt-1 ml-2">Files: {item.files.length}</p>
                              </div>
                              <div className="col-sm-6 text-right">
                                <Link className="btn btn-sm btn-primary rounded-0" to={`folders/${item._id}`}>View</Link>
                                {item.files.length == 0 && <button className="btn btn-sm btn-danger rounded-0" onClick={this.handleDeleteFolder.bind(this, item._id)}>Delete</button>}
                              </div>
                            </div>
                          </div>
                        </div> */}
                      </Card>
                    )
                  })}
                  </div>  
                </Card>
              </div>
            {/* </Body> */}
        </div>
    );
  }
}
