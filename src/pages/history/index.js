import React, { Component } from 'react';
import "./styles.css";
import Card from "../../components/card.component"
import Tabs from "../../components/tabs.component"
import TabContent from "../../components/tabs/tabContent.component"

export default class Main extends Component {

  constructor(){
    super();
    this.state = {}
  }

  render() {

    return (
      <div className="">
        <div className="row">
          <Card size="col-sm-12" title="History" icon={"dataUsage"} iconColor={"#212529"} iconSize={40}>
            <Tabs id={"nav-id"}
              tabLinks={[
                {title:"Elementary Scholl", id:"elementary-scholl", active: true, labelledby:"nav-elementary-scholl"},
                {title:"High Scholl", id: "high-scholl", labelledby:"nav-high-scholl"},
                {title:"University", id: "university", labelledby:"nav-university"}
              ]}>
              <TabContent show={true} active={true} id={"elementary-scholl"} labelledby={"nav-elementary-scholl"}>
                Content Here...
              </TabContent>
              <TabContent show={false} active={false} id={"high-scholl"} labelledby={"nav-high-scholl"}>
                Content Here...
              </TabContent>
              <TabContent show={false} active={false} id={"university"} labelledby={"nav-university"}>
                Content Here...
              </TabContent>
            </Tabs>
          </Card>
        </div>
      </div>
    ); 
  }
}
