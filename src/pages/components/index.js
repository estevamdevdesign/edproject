import React, { Component } from 'react';
import Body from "../../components/body.component";
import "./styles.css";
import Card from "../../components/card.component"
import Chart from "../../components/chart.component"

const data =[
  {name: 'Page A', uv: 4000, pv: 2400, amt: 2400,},
  {name: 'Page B', uv: 3000, pv: 1398, amt: 2210,},
  {name: 'Page C', uv: 2000, pv: 9800, amt: 2290,},
  {name: 'Page D', uv: 2780, pv: 3908, amt: 2000,},
  {name: 'Page E', uv: 1890, pv: 4800, amt: 2181,},
  {name: 'Page F', uv: 2390, pv: 3800, amt: 2500,},
  {name: 'Page G', uv: 3490, pv: 4300, amt: 2100,},
  {name: 'Page H', uv: 3490, pv: 4300, amt: 2100,},
];

const data2 = [
  { name: 'Group A', value: 400 },
  { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 },
  { name: 'Group D', value: 200 },
];

export default class Folders extends Component {
  
  constructor(){
    super();
    this.state = {title: "", files: []};
  }

  render() {
    return (
        <div className="">
            {/* <Body> */}
              <div className="row">
              <Card size="col-sm-12" title="Components" icon={"build"} iconColor={"#212529"} iconSize={40}>
                <div className="row">
                  <div className="col-sm-3">
                    <ul className="navigation">
                      <li><a href="#cards">Cards</a></li>
                      <li><a href="#charts">Charts</a></li>
                      <li><a href="#form">Form Components</a></li>
                      <li><a href="#icons">Icons</a></li>
                      <li><a href="#menu">Menu Navigation</a></li>
                      <li><a href="#table">Table</a></li>
                    </ul>
                  </div>
                  <div className="col-sm-9">
                    <Card size="col-sm-12" title="Cards" id="cards">
                      <p>This is a card component this application.</p>
                      <h2>Examples</h2>
                      <div className="row">
                        <Card title="Card" icon={"dataUsage"} iconColor={"#212529"} iconSize={20} size="col-sm-12" titleSize={20} subTitle={"This a subTitle"}>
                          <p>This is a card with own content and style.</p>
                          <button className="btn main-btn">Submit</button>
                        </Card>
                        <Card title="Card" icon={"dataUsage"} iconColor={"#212529"} iconSize={20} size="col-sm-6" bgColor="main-bg" titleSize={20} subTitle={"This a subTitle"} color="text-white" subColor="text-white" iconColor="#fff">
                          <p className="text-white">This is a card with own content and style.</p>
                          <button className="btn btn-danger">Submit</button>
                        </Card>
                        <Card title="Card" icon={"dataUsage"} iconColor={"#212529"} iconSize={20} size="col-sm-6" bgColor="main-bg" titleSize={20} subTitle={"This a subTitle"} color="text-white" subColor="text-white" iconColor="#fff">
                          <p className="text-white">This is a card with own content and style.</p>
                          <button className="btn btn-danger">Submit</button>
                        </Card>
                        <Card title="Card" icon={"dataUsage"} iconColor={"#212529"} iconSize={20} size="col-sm-3" bgColor="main-bg" titleSize={20} subTitle={"This a subTitle"} color="text-white" subColor="text-white" iconColor="#fff">
                          <p className="text-white">This is a card with own content and style.</p>
                          <button className="btn btn-danger">Submit</button>
                        </Card>
                        <Card title="Card" icon={"dataUsage"} iconColor={"#212529"} iconSize={20} size="col-sm-3" bgColor="main-bg" titleSize={20} subTitle={"This a subTitle"} color="text-white" subColor="text-white" iconColor="#fff">
                          <p className="text-white">This is a card with own content and style.</p>
                          <button className="btn btn-danger">Submit</button>
                        </Card>
                        <Card title="Card" icon={"dataUsage"} iconColor={"#212529"} iconSize={20} size="col-sm-3" bgColor="main-bg" titleSize={20} subTitle={"This a subTitle"} color="text-white" subColor="text-white" iconColor="#fff">
                          <p className="text-white">This is a card with own content and style.</p>
                          <button className="btn btn-danger">Submit</button>
                        </Card>
                        <Card title="Card" icon={"dataUsage"} iconColor={"#212529"} iconSize={20} size="col-sm-3" bgColor="main-bg" titleSize={20} subTitle={"This a subTitle"} color="text-white" subColor="text-white" iconColor="#fff">
                          <p className="text-white">This is a card with own content and style.</p>
                          <button className="btn btn-danger">Submit</button>
                        </Card>
                      </div>
                    </Card>
                    <Card size="col-sm-12" title="Charts" id="charts">
                      <p>This is a chart component this application.</p>
                      <h2>Examples</h2>
                      <div className="row">
                        <Chart size="col-sm-12" 
                          title="Bar Chart"
                          subTitle="Today"
                          bgColor="theme-bg-1"
                          chart="BarChart"
                          chartData={data}
                          chartHeight={200}
                          chartTooltip={true}
                          chartLegend={true}
                          chartFillColor="#fff"
                          chartDataKey="pv"
                          haveFilter={true}
                          color="text-white"
                          subColor="text-white">
                        </Chart>
                        <Chart size="col-sm-6"
                          title="Line Chart"
                          bgColor="theme-bg-2"
                          subTitle="Today"
                          chart="LineChart"
                          chartData={data}
                          chartHeight={200}
                          chartTooltip={true}
                          chartLegend={true}
                          chartDataKeyList ={[{filter: 'uv', color: "#fff"}, {filter: 'pv', color: "#8069f2"}]}
                          haveFilter={true}
                          color="text-white"
                          subColor="text-white">
                        </Chart>
                        <Chart size="col-sm-6" 
                          title="Pie Chart"
                          subTitle="Today"
                          bgColor="bg-white"
                          chart="PieChart"
                          chartData={data2}
                          chartHeight={200}
                          chartColor={['#8069f2', '#6999f2', '#69c6f2', '#0cdcB9']}
                          haveFilter={false}
                          subColor="text-second">
                        </Chart>
                      </div>
                    </Card>
                    <Card size="col-sm-12" title="Form Components" id="form">

                    </Card>
                  </div>
                </div>
              </Card>
              </div>
            {/* </Body> */}
        </div>
    );
  }
}
