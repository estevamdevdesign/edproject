import React, { Component } from 'react';
import "./styles.css";
import crud from "../../services/crud";
import {Redirect, Link} from "react-router-dom";
import Title from "../../components/title.component";

export default class Folders extends Component {
  
  constructor(){
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      saved: false
    };
  }

  handleSubmit(event){
    event.preventDefault()
    crud.post("users", this.state).then(() =>{
      this.setState({saved: true})
    }).catch((e) =>{
      console.log(e)
    })

  }

  handleName = (event) =>{
    this.setState({name: event.target.value})
  }

  handleEmail = (event) =>{
    this.setState({email: event.target.value})
  }

  handlePassword = (event) =>{
    this.setState({password: event.target.value})
  }

  render() {

    if(this.state.saved){
      return (
        <Redirect to="/" />
      )
    }else{
      return (
        <div className="">
          <div className="container pt-5">
            <div className="row pt-5">
              <form className="col-sm-4 offset-sm-4 bg-white p-5" onSubmit={this.handleSubmit.bind(this)}>
                <Title/>
                <div className="form-group">
                  <label className="small">Name</label>
                  <input className="form-control col-sm-12" name="name" onChange={this.handleName}/>
                </div>
                <div className="form-group">
                  <label className="small">Email</label>
                  <input className="form-control col-sm-12" type="email" onChange={this.handleEmail}/>
                </div>
                <div className="form-group">
                  <label className="small">Password</label>
                  <input className="form-control col-sm-12" type="password" onChange={this.handlePassword}/>
                </div>
                <div className="form-group">
                  <button className="btn main-btn"> Sing Up</button>
                  <div className="float-right">
                    <Link to={"/"} className="ml-2 main-text">Back</Link>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }
}
