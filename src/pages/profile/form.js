const options = [
  { value: 'DF', label: 'Distrito Federal' },
  { value: 'SP', label: 'São Paulo' },
  { value: 'RS', label: 'Rio Grande do Sul' },
]

export default [

  //size : 'col-*-1', 'col-*-2', ... , 'col-*-12'
  //name : String
  //id : String
  //disabled : boolean
  //readOnly : boolean
  //placeholder
  //type : 'text', 'date', 'email', 'password', 'textarea', 'radio'
  //label : String
  //dependency : String
  //dataBind : Array ([{field: "field_name", value: "key_value_name"}])
  //mask : String
  //options : Array
  //value : String, Int, boolean...

  {label: "Name", type: "text", required: false, id: "name", name: "name", size:"col-sm-4"},
  {label: "Last name", type: "text", required: false, id: "lastName", name: "lastName", size:"col-sm-6"},
  {label: "Birthday", type: "date", required: false, id: "birthDay", name: "birthDay", size:"col-sm-2"},
  {type: "space", size:"col-sm-12", fieldTitle: "Address"},
  {label: "ZIP code", type: "text", mask: "99999-999", required: false, id: "zipCode", name: "zipCode", size:"col-sm-2"},
  {label: "Address", type: "text", required: false, id: "address", name: "address", size:"col-sm-8", },
  {label: "Number", type: "text", required: false, id: "number", name: "number", size:"col-sm-2"},
  {label: "Neighborhood", type: "text", required: false, id: "neighborhood", name: "neighborhood", size:"col-sm-3"},
  {label: "Complement", type: "text", required: false, id: "complement", name: "complement", size:"col-sm-3"},
  {label: "State", type: "autoComplete", options: options, apiOptions: "states", required: false, id: "state", name: "stateSelect", size:"col-sm-2"},
  {label: "City", type: "text", required: false, id: "city", name: "city", size:"col-sm-4"},
  {type: "space", size:"col-sm-12", fieldTitle: "User data"},
  {label: "Username", type: "text", required: false, id: "username", name: "username", size:"col-sm-4"},
  {label: "Email", type: "email", required: false, id: "email", name: "email", size:"col-sm-4"},
  {label: "Password", type: "password", required: false, id: "password", name: "password", size:"col-sm-4"},
  {type: "space", size:"col-sm-12"},
  {label: "Description", type: "textarea", required: false, id: "description", name: "description", size:"col-sm-12"},
]