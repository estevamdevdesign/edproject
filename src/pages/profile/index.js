import React, { Component } from 'react';
import Card from "../../components/card.component";
import "./styles.css";
import Form from  "../../components/form.component";
import Input from  "../../components/form/input.component";
import InputMask from  "../../components/form/inputMask.component";
import AutoComplete from  "../../components/form/autoComplete.component";
import Radio from  "../../components/form/radio.component";
import Divider from "../../components/form/divider.component"
import AsyncAutoComplete from  "../../components/form/asyncAutoComplete.component";
import TextArea from  "../../components/form/textarea.component";
import SwitchButton from '../../components/form/switchButton.component';
import crud from '../../services/crud';

const options = [
  { value: 'DF', label: 'Distrito Federal' },
  { value: 'SP', label: 'São Paulo' },
  { value: 'RS', label: 'Rio Grande do Sul' },
]

export default class Profile extends Component {

  constructor(){
    super();
    this.state = {
      name:'',
      lastName:'',
      genre:'',
      stateSelected:'',
      city:'',
      birthDay:'',
      userName:'',
      email:'',
      adminUser:'',
      neighborhood:'',
      address:'',
      number:'',
      zipCode:'',
    }
  }

  handleSelect(newValue, name, bind){
    this.setState({[name] : newValue}, () =>{
        bind && bind.map((data) =>{
            this.setState({[data.field] : newValue[data.value]})
        })
    })
  }

  async loadProfileData(userId){
    await crud.get(`users/${userId}`).then((response) =>{
      this.setState({
        name:response.data.name,
        lastName: response.data.lastName,
        genre: response.data.genre,
        stateSelected: response.data.stateSelected,
        city: response.data.city,
        birthDay: response.data.birthDay,
        userName: response.data.userName,
        email: response.data.email,
        adminUser: response.data.adminUser,
        neighborhood: response.data.neighborhood,
        address: response.data.address,
        number: response.data.number,
        zipCode: response.data.zipCode,

      })
    })
  }

  componentWillMount(){
    if(this.props && this.props.user_id){
      this.loadProfileData(this.props.user_id)
    }
  }

  
  render() {

    return (
        <div className="">
              <div className="row">
                <Card size="col-sm-12" title="Profile" icon={"accountCircle"} iconColor={"#212529"} iconSize={40}>
                  <Form
                    submitButton={"Submit form"}
                    deleteButton={"Delete data"}
                    iconSubmit={"done"}
                    iconDelete={"clear"}
                    className="container-fluid"
                    data={this.state}
                    iconColor={"#fff"}
                    api={"users"}
                    redirect={"dashboard"}
                    >

                    <Input size={'col-sm-4'} id={'name'} name={'name'} value={this.state && this.state['name']}
                      required={false}
                      readOnly={false}
                      disabled={false}
                      dependency={false}
                      type={'text'}
                      onChange={(event) => this.setState({['name'] : event.target.value})}
                      label={'Name'}
                    />
                    <Input size={'col-sm-6'} id={'lastName'} name={'lastName'} value={this.state && this.state['lastName']}
                        required={false}
                        readOnly={false}
                        disabled={false}
                        type={'text'}
                        disabled={!this.state['name']}
                        onChange={(event) => this.setState({['lastName'] : event.target.value})}
                        label={'Last name'}
                    />
                    
                    <InputMask
                      mask={'99/99/9999'}
                      size={'col-sm-2'}
                      id={'birthDay'}
                      name={'birthDay'}
                      value={this.state && this.state['birthDay']}
                      required={false}
                      readOnly={false}
                      onChange={(event) => this.setState({['birthDay'] : event.target.value})}
                      label={'Birth day'}
                    />
                    <div className="col-sm-4 bg-light">
                      <label className="label-form">Genre</label>
                      <div className="row mt-2">
                        <Radio size={'col-sm-6'} id={'genreF'} name={'genre'} value={this.state && this.state['genre']}
                          required={false}
                          readOnly={false}
                          disabled={false}
                          dependency={false}
                          checked={this.state['genre'] == 'Female'}
                          onChange={(event) => this.setState({['genre'] : 'Female'})}
                          label={'Female'}
                        />

                        <Radio size={'col-sm-6'} id={'genreM'} name={'genre'} value={this.state && this.state['genre']}
                          required={false}
                          readOnly={false}
                          disabled={false}
                          dependency={false}
                          checked={this.state['genre'] == 'Male'}
                          onChange={(event) => this.setState({['genre'] : 'Male'})}
                          label={'Male'}
                        />
                      </div>
                    </div>

                    <Divider dividerTitle="Address" />

                    {/* <AutoComplete
                      size={'col-sm-4'}
                      options={options}
                      // defaultValue={1}
                      name={'stateSelected'}
                      id={'stateSelected'}
                      value={this.state && this.state['stateSelected']}
                      // required={false}
                      // disabled={!this.state['name']}
                      onChange={(newValue) =>{this.handleSelect(newValue, 'stateSelected', [{field: "city", value: "label"}])}}
                      label={'State'}
                    /> */}

                    <InputMask
                      mask={'99999-999'}
                      size={'col-sm-2'}
                      id={'zipCode'}
                      name={'zipCode'}
                      value={this.state && this.state['zipCode']}
                      required={false}
                      readOnly={false}
                      onChange={(event) => this.setState({['zipCode'] : event.target.value})}
                      label={'Zip code'}
                    />

                    <Input size={'col-sm-6'} id={'address'} name={'address'} value={this.state && this.state['address']}
                      required={false}
                      readOnly={false}
                      disabled={false}
                      type={'text'}
                      onChange={(event) => this.setState({['address'] : event.target.value})}
                      label={'Address'}
                    />

                    <Input size={'col-sm-2'} id={'number'} name={'number'} value={this.state && this.state['number']}
                      required={false}
                      readOnly={false}
                      disabled={false}
                      type={'text'}
                      onChange={(event) => this.setState({['number'] : event.target.value})}
                      label={'Number'}
                    />

                    <Input size={'col-sm-2'} id={'complement'} name={'complement'} value={this.state && this.state['complement']}
                      required={false}
                      readOnly={false}
                      disabled={false}
                      type={'complement'}
                      onChange={(event) => this.setState({['complement'] : event.target.value})}
                      label={'Complement'}
                    />

                    <Input size={'col-sm-4'} id={'neighborhood'} name={'neighborhood'} value={this.state && this.state['neighborhood']}
                      required={false}
                      readOnly={false}
                      disabled={false}
                      type={'neighborhood'}
                      onChange={(event) => this.setState({['neighborhood'] : event.target.value})}
                      label={'Neighborhood'}
                    />

                    <AsyncAutoComplete
                      size={'col-sm-4'}
                      name={'stateSelected'}
                      id={'stateSelected'}
                      value={this.state && this.state['stateSelected']}
                      data={'states'}
                      onChange={(newValue) =>{this.handleSelect(newValue, 'stateSelected', [{field: "city", value: "label"}])}}
                      label={'State'}
                    />

                    <Input size={'col-sm-4'} id={'city'} name={'city'} value={this.state && this.state['city']}
                      required={false}
                      readOnly={false}
                      disabled={this.state.stateSelected && !this.state.stateSelected.value}
                      type={'text'}
                      onChange={(event) => this.setState({['city'] : event.target.value})}
                      label={'City'}
                    />

                    <Divider dividerTitle="User data" />

                    {/* <TextArea
                      size={'col-sm-12'}
                      id={'description'}
                      name={'description'}
                      value={this.state && this.state['description']}
                      required={false}
                      readOnly={false}
                      disabled={false}
                      height={100}
                      onChange={(event) => this.setState({['description'] : event.target.value})}
                      label={'Description'}
                    /> */}
                    <Input size={'col-sm-4'} id={'userName'} name={'userName'} value={this.state && this.state['userName']}
                      required={true}
                      readOnly={false}
                      disabled={false}
                      type={'text'}
                      onChange={(event) => this.setState({['userName'] : event.target.value})}
                      label={'Username'}
                    />

                    <Input size={'col-sm-4'} id={'email'} name={'email'} value={this.state && this.state['email']}
                      required={false}
                      readOnly={false}
                      disabled={false}
                      type={'text'}
                      onChange={(event) => this.setState({['email'] : event.target.value})}
                      label={'Email'}
                    />

                    <SwitchButton
                      label={'Is you Admin User?'}
                      activeColor={'#8069f2'}
                      size={'col-sm-4'}
                      true={'Yes'}
                      false={'No'}
                      value={this.state && this.state['adminUser']}
                      onTrueClick={() => this.setState({['adminUser']: true})}
                      onFalseClick={() => this.setState({['adminUser']: false})}
                    />

                  </Form>
                </Card>
              </div>
        </div>
    );
  }
}
