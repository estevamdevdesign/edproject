import React, { Component } from 'react';
import "./styles.css";
import Card from "../../components/card.component"

export default class Folders extends Component {
  
  constructor(){
    super();
    this.state = {title: "", files: []};
  }

  render() {

    console.log(this.props)

    return (
        <div className="">
          <div className="row">
            <Card size="col-sm-12" title="Plugins" icon={"build"} iconColor={"#212529"} iconSize={40}>
              <div className="row">
                <div className="col-sm-12">
                  <p>Plugins list of ED Project</p>
                </div>
              </div>
              <div className="row">
                  {this.props.plugins.map((plugin) =>{
                    return (
                      <Card size="col-sm-4" title={plugin.name}>
                        <p>{plugin.plugin_description}</p>
                      </Card>
                    )
                  })}
                </div>
            </Card>
          </div>
        </div>
    );
  }
}
