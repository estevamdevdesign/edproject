import React, { Component } from 'react';
import "./styles.css";
import crud from "../../services/crud";
import Title from "../../components/title.component"
import {Redirect, Link} from "react-router-dom";

export default class Folders extends Component {
  
  constructor(){
    super();
    this.state = {
      email: "",
      password: "",
      redirect: false,
      error: ""
    };
  }

  handleSubmit(event){
    event.preventDefault()
    crud.getWithBody("login", this.state).then((response) =>{
      if(response.data.email){
        localStorage.setItem("user_id", response.data._id)
      }else{
        this.setState({redirect: false, error: response.data})
      }
    }).then(() =>{
      this.setState({redirect: true})
    }).catch((e) =>{
      console.log(e)
    })
  }

  handleEmail = (event) =>{
    this.setState({email: event.target.value})
  }

  handlePassword = (event) =>{
    this.setState({password: event.target.value})
  }

  render() {
    if(this.state.redirect){
      return (
        <Redirect to="/dashboard" />
      )
    }else{
      return (
        <div className="">
          <div className="container pt-5">
            <div className="row pt-5">
              <div className="col-sm-3 offset-sm-2 main-bg text-center d-flex align-items-center justify-content-center">
                <Title color="#fff"/>
              </div>
              <form className="col-sm-5 bg-white p-5" onSubmit={this.handleSubmit.bind(this)}>
                {(this.state.error || this.props.error) && 
                  <p className="alert alert-danger">{this.state.error ? this.state.error : this.props.error}</p>
                }
                <div className="form-group">
                  <label className="small main-text font-weight-bold">Email</label>
                  <input className="form-control col-sm-12" type="email" onChange={this.handleEmail}/>
                </div>
                <div className="form-group">
                  <label className="small main-text font-weight-bold">Password</label>
                  <input className="form-control col-sm-12" type="password" onChange={this.handlePassword}/>
                </div>
                <div className="form-group">
                  <button className="btn main-btn"> Login</button>
                  <div className="float-right">
                    <Link to={"/register"} className="ml-2 main-text">Create account</Link>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }
}
