import axios from "axios";
import url from "./url";

var services = {
    get: (urlBase) =>{
        return axios.get(`${url}${urlBase}`)
    },

    getAll: (id, urlBase) =>{
        return axios.get(`${url}${urlBase}/${id}`)
    },

    getWithBody: (urlBase, data) =>{
        return axios.post(`${url}${urlBase}`, data)
    },
    
    post: (urlBase, data) =>{
        return axios.post(`${url}${urlBase}`, data)
    },
    put: (id, urlBase, data) =>{
        
    },
    delete: (id, urlBase) =>{
        return axios.delete(`${url}${urlBase}/${id}`)
    }

}

export default services;