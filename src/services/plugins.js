import React, { Component } from 'react';

const plugins = {
    "fileManager": (color, size) =>{
        return (
            <MdInsertDriveFile size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    }
}

export default plugins;