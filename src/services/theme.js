


let main_color = "#8069f2";
let theme_color = "#6999f2";
let theme_color_2 = "#69c6f2";
let theme_color_3 = "#0cdcB9";
let dark_color = "#242944";
let deep_color = "#0f111d";

export default {
    mainBg : {backgroundColor: main_color},
    theme_bg_1: {backgroundColor: theme_color},
    theme_bg_2: {backgroundColor: theme_color_2},
    theme_bg_3: {backgroundColor: theme_color_3},
    mainText : {color: main_color},
    theme_text_1 : {color: main_color},
    theme_text_2 : {color: theme_color},
    theme_text_3 : {color: theme_color_2},
    darkBg : {backgroundColor: dark_color},
    deepBg : {backgroundColor: deep_color},
    mainBtn : {color: "#fff", border:  "0px solid " + main_color,backgroundColor: main_color},
    darkBtn : {color: "#fff", border: "0px solid " + dark_color, backgroundColor: dark_color},
    navLink : {
        color: main_color,
        padding: '15px',
        display: 'inherit',
        cursor: 'pointer',
        textDecoration: 'none',
        borderRight: '3px solid transparent',
        ':hover': {
            color: "#ffcc00",
            borderRightColor: theme_color_3 
        }
    }
}