import React, { Component } from 'react';
import {MdInsertDriveFile,
    MdPlusOne,
    MdPieChart,
    MdAdd,
    MdFolder,
    MdCreateNewFolder,
    MdFolderOpen,
    MdDataUsage,
    MdMenu,
    MdSettings,
    MdBuild,
    MdMoreVert,
    MdMoreHoriz,
    MdRemove,
    MdAccountCircle,
    MdClear,
    MdDone,
    MdDashboard

} from 'react-icons/md';

const defaultColor = "#8069f2"
const defaultSize = 20

const icons = {
    "file": (color, size) =>{
        return (
            <MdInsertDriveFile size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "folderOpen": (color, size) =>{
        return (
            <MdFolderOpen size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "folder": (color, size) =>{
        return (
            <MdFolder size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "plusOne": (color, size) =>{
        return (
            <MdPlusOne size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "pieChart": (color, size) =>{
        return (
            <MdPieChart size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "createNewFolder": (color, size) =>{
        return (
            <MdCreateNewFolder size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "dataUsage": (color, size) =>{
        return (
            <MdDataUsage size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "menu": (color, size) =>{
        return (
            <MdMenu size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "settings": (color, size) =>{
        return (
            <MdSettings size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "add": (color, size) =>{
        return (
            <MdAdd size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "build": (color, size) =>{
        return (
            <MdBuild size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "moreVert": (color, size) =>{
        return (
            <MdMoreVert size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "moreHoriz": (color, size) =>{
        return (
            <MdMoreHoriz size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "remove": (color, size) =>{
        return (
            <MdRemove size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "accountCircle": (color, size) =>{
        return (
            <MdAccountCircle size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "done": (color, size) =>{
        return (
            <MdDone size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "clear": (color, size) =>{
        return (
            <MdClear size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },
    "dashboard": (color, size) =>{
        return (
            <MdDashboard size={size ? size : defaultSize} color={color ? color : defaultColor} />
        )
    },

}

export default icons;