import React from 'react';
import Profile from './pages/profile';
import Adapter from 'enzyme-adapter-react-16';
import { create, update } from 'react-test-renderer';
import {shallow, mount, render, configure } from 'enzyme';
import crud from './services/crud'

configure({adapter: new Adapter()});

describe("Profile Component render without errors", () =>{
  test('renders correctly without input values', () =>{
    const page = create(<Profile/>).toJSON();
    expect(page).toMatchSnapshot()
  })
})

describe("Profile component change input value and state", () =>{
  test("Input Profile Name", () =>{
    const page = shallow(<Profile/>);
    const name = page.find('#name');

    name.simulate('change', { target: { value: 'Rafael' } });

    expect(page.state().name).toEqual('Rafael');
    expect(page).toMatchSnapshot()

  })
})

describe("Profile component loaded with values in state using a user_id", () =>{
  test("Input Profile Name", () =>{
    const page  = shallow(<Profile />);
    const name = page.find('#name');

    return crud.get(`users/5ccce62457c33c001764f035`).then((response) =>{
      name.simulate('change', {target: {value: response.data.name}})
    }).then(() =>{
      expect(page.state().name).toEqual("Rafael")
      expect(page).toMatchSnapshot()
    })
  })
})
