import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import Folders from './pages/folders';
import Folder from './pages/folder';
import Register from './pages/register';
import Login from './pages/login';
import AddFolder from './pages/add-folder'
import Dashboard from './pages/dashboard'
import Component from './pages/components'
import Profile from './pages/profile'
import FourHundredFour from './pages/404'
import plugins from './plugins'
import "./css/main.css"

let getUser;

function getUserAuth (next, replace){
    getUser = localStorage.getItem('user_id');
    if(getUser != null){
        return true;
    }else{
        return true;
        localStorage.setItem('msg-auth', 'Você precisa logar para acessar a timeline')
    }
}

function redirectTo(link, props){
    
    switch(link){
        case "AddFolder" :
            return getUserAuth() ? (<AddFolder {...props} user_id={getUser}/>) : <Redirect to="/"/>
        case "Dashboard" :
            return getUserAuth() ? (<Dashboard {...props} user_id={getUser}/>) : <Redirect to="/"/>
        case "Folders" :
            return getUserAuth() ? (<Folders {...props} user_id={getUser}/>) : <Redirect to="/"/>
        case "Folder" :
            return getUserAuth() ? (<Folder {...props} user_id={getUser}/>) : <Redirect to="/"/>
        case "Components" :
            return getUserAuth() ? (<Component {...props} user_id={getUser}/>) : <Redirect to="/"/>
        case "Profile" :
            return getUserAuth() ? (<Profile {...props} user_id={getUser}/>) : <Redirect to="/"/>
        case "Emails":
            return plugins.email.emails(props, getUser)
        case "Email":
            return plugins.email.email(props, getUser)
        default :
            return <Login/>
    }
}


ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={Login} />
            <Route path="/register" exact={true} component={Register} />

            <Route path="/dashboard" exact={true} render={(props) => redirectTo("Dashboard", props)}/>
            <Route path="/add-folders" exact={true} render={(props) => redirectTo("AddFolder", props)}/>
            <Route path="/folders" exact={true} render={(props) => redirectTo("Folders", props)} />
            <Route path="/folders/:id" exact={true} render={(props) => redirectTo("Folder", props)} />
            <Route path="/components" exact={true} render={(props) => redirectTo("Components", props)} />
            <Route path="/profile" exact={true} render={(props) => redirectTo("Profile", props)} />

            <Route path="/emails" exact={true} render={(props) => redirectTo("Emails", props)} />
            <Route path="/email" exact={true} render={(props) => redirectTo("Email", props)} />

            <Route path="*" exact={true} component={FourHundredFour} />
        </Switch>
    </BrowserRouter>,
    document.getElementById('root')
);

